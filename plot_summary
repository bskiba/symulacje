#!/bin/bash

# Script that accumulates results from all output files placed in given directory and plots the averaged result (fitness by step)

if [[ ! -d "$1" ]]; then
    echo "Specify directory containing fitness files to plot."
    exit 1
fi


BEST_FILES=`ls -1 $1 | grep '_best' | tr "\n" " "`

LAST_BEST=`echo $BEST_FILES | awk '{print $NF}'`
LINES=`cat $1/$LAST_BEST | wc -l`

NUM_FILES=`echo $BEST_FILES | wc -w`


if [[ "$2" != "skip_calc" ]]; then

    # Produce summary of best fitness files
    rm -f "$1/best_summary.txt"

    COUNTER=0    

    for line in `seq 1 $LINES`
    do
        MIN=100
        MAX=-1
        SUM=0
        for file in $BEST_FILES
        do        
            NUM=`sed -n "${line}p" < $1/$file | awk '{print $NF}'`
            if [[ $NUM =~ ^[0-9]+\.[0-9]+$ ]]; then
                SUM=`echo "$SUM + $NUM" | bc`
                if [[ `echo "$NUM < $MIN" | bc` -eq 1 ]]; then
                    MIN=$NUM
                fi
                if [[ `echo "$NUM > $MAX" | bc` -eq 1 ]]; then
                    MAX=$NUM
                fi
            fi
        done
        if [[ `echo "$SUM == 0" | bc` -eq 1 ]]; then        
            echo `sed -n "${line}p" < $1/$file` >> $1/best_summary.txt
        else        
            AVG=`echo "scale=10; $SUM / $NUM_FILES" | bc`    
            echo "$COUNTER $AVG $MIN $MAX" >> $1/best_summary.txt
            COUNTER=$((COUNTER + 1))
        fi
    done



    # Produce summary of avg fitness files
    rm -f "$1/avg_summary.txt"

    AVG_FILES=`ls -1 $1 | grep '_avg' | tr "\n" " "`

    COUNTER=0

    for line in `seq 1 $LINES`
    do
        MIN=100
        MAX=-1
        SUM=0
        for file in $AVG_FILES
        do        
            NUM=`sed -n "${line}p" < $1/$file | awk '{print $NF}'`
            if [[ $NUM =~ ^[0-9]+\.[0-9]+$ ]]; then
                SUM=`echo "$SUM + $NUM" | bc`
                if [[ `echo "$NUM < $MIN" | bc` -eq 1 ]]; then
                    MIN=$NUM
                fi
                if [[ `echo "$NUM > $MAX" | bc` -eq 1 ]]; then
                    MAX=$NUM
                fi
            fi
        done
        if [[ `echo "$SUM == 0" | bc` -eq 1 ]]; then        
            echo `sed -n "${line}p" < $1/$file` >> $1/avg_summary.txt
        else        
            AVG=`echo "scale=10; $SUM / $NUM_FILES" | bc`    
            echo "$COUNTER $AVG $MIN $MAX" >> $1/avg_summary.txt
            COUNTER=$((COUNTER + 1))
        fi
    done
fi


TITLE=$(head -n 1 $1/${LAST_BEST})
TITLE_PROPER=`echo $TITLE | sed 's/^..//'`

gnuplot -p -e "filename_best='$1/best_summary.txt'" \
    -e "filename_avg='$1/avg_summary.txt'"  -e "plot_title='${TITLE_PROPER} - summary of ${NUM_FILES} runs.'" gnuplot_summary_config.cfg