import logging
import time
import sys
from pyage.core.statistics import Statistics

logger = logging.getLogger(__name__)

# Zbiera statystyki z symulacji. Pozwala to
# - okreslic progres symulacji
# - zbierac wiedze o rozkladzie fitnessu w genotypach na poszczegolnych wyspach (potrzebne do mutacji COMMA)
# - zapisywac wyniki do pliku
class LabsStatistics(Statistics):
    # Ta zmienna statyczna pozwoli nam znac max fitness globalnie
    max_fitness = 0;

    def __init__(self, step_limit, header='LABS COMMA', output_file_name='fitness_pyage'):
        self.history = []
        self.fitness_output = open('results/' + output_file_name + '_best.txt', 'a')
        self.avg_fitness_output = open('results/' + output_file_name + '_avg.txt', 'a')
        self.fitness_output.write("# %s\n" % header)
        self.avg_fitness_output.write("# [AVERAGE] %s\n" % header)
        self.step_limit = step_limit
        # Draw progress bar
        print "0% ========================================== 100%"

    def __del__(self):
        self.fitness_output.close()
        self.avg_fitness_output.close()

    def append(self, step_count, best_fitness, avg_fitness):
        self.fitness_output.write(str(step_count - 1) + ' ' + str(abs(best_fitness)) + '\n')
        self.avg_fitness_output.write(str(step_count - 1) + ' ' + str(abs(avg_fitness)) + '\n')

    def update(self, step_count, agents):
        self.draw_progress(step_count, self.step_limit),
        try:
            for a in agents:
                agents_on_island = a._AggregateAgent__agents.values()
                #print len(agents_on_island),
                max_on_island = agents_on_island[0].get_fitness();   
                min_on_island = max_on_island
               
                for agent in agents_on_island:
                    fitness = agent.get_fitness()
                    min_on_island = min(fitness, min_on_island)
                    max_on_island = max(fitness, max_on_island)
                for agent in agents_on_island:
                    agent.genotype.max_fitness = max_on_island
                    agent.genotype.min_fitness = min_on_island
            #print ""
            best_fitness = max(a.get_fitness() for a in agents)
            avg_fitness = avg(agents)
            logger.info(best_fitness)
            self.history.append(best_fitness)
            self.append(step_count, best_fitness, avg_fitness)

            # Set globally known max fitness, if it improved
            if (best_fitness > LabsStatistics.max_fitness):
                LabsStatistics.max_fitness = best_fitness

        except:
            logging.exception("")
            print sys.exc_info()[0]

    def summarize(self, agents):
        print
        try:
            logger.debug(self.history)
            logger.debug("best genotype: %s", max(agents, key=lambda a: a.get_fitness()).get_best_genotype())
            print "best genotype: %s" % (max(agents, key=lambda a: a.get_fitness())).get_best_genotype()
        except:
            logging.exception("")

    # Draw progress on the progress bar
    def draw_progress(self, current_step, all_steps):
        if (current_step % int((all_steps / 50)) == 0):
            sys.stdout.write("^")
            sys.stdout.flush()


# Zbiera statystyki z symulacji, dla algorytmu ewolucyjnego. Pozwala to
# - okreslic progres symulacji
# - zbierac wiedze o rozkladzie fitnessu w genotypach na poszczegolnych wyspach (potrzebne do mutacji COMMA)
# - zapisywac wyniki do pliku
class LabsEvoStatistics(Statistics):

    def __init__(self, step_limit, header='LABS COMMA', output_file_name='fitness_pyage'):
        self.history = []
        self.fitness_output = open('results/' + output_file_name + '_best.txt', 'a')
        self.avg_fitness_output = open('results/' + output_file_name + '_avg.txt', 'a')
        self.fitness_output.write("# %s\n" % header)
        self.avg_fitness_output.write("# [AVERAGE] %s\n" % header)
        self.step_limit = step_limit
        # Draw progress bar
        print "0% ========================================== 100%"

    def __del__(self):
        self.fitness_output.close()
        self.avg_fitness_output.close()

    def append(self, step_count, best_fitness, avg_fitness):
        self.fitness_output.write(str(step_count - 1) + ' ' + str(abs(best_fitness)) + '\n')
        self.avg_fitness_output.write(str(step_count - 1) + ' ' + str(abs(avg_fitness)) + '\n')

    def update(self, step_count, agents):
        self.draw_progress(step_count, self.step_limit),
        try:
            best_fitness = max(a.get_fitness() for a in agents)
            avg_fitness = 0
            total_population_size = 0
            for a in agents:
                total_population_size += len(a.population)
                for genotype in a.population:
                    # Czasem sa None, nie wiem jeszcze dlaczego, ale na razie
                    # po prostu wywalam je z liczenia sredniej
                    if genotype.fitness:
                        avg_fitness += genotype.fitness
                    else:
                        total_population_size -=1
            avg_fitness /= total_population_size * 1.0
            logger.info(best_fitness)
            self.history.append(best_fitness)
            self.append(step_count, best_fitness, avg_fitness)
        except:
            logging.exception("")
            print sys.exc_info()[0]

    def summarize(self, agents):
        print
        try:
            logger.debug(self.history)
            logger.debug("best genotype: %s", max(agents, key=lambda a: a.get_fitness()).get_best_genotype())
            print "best genotype: %s" % max(agents, key=lambda a: a.get_fitness()).get_best_genotype()
        except:
            logging.exception("")

    # Draw progress on the progress bar
    def draw_progress(self, current_step, all_steps):
        if (current_step % int((all_steps / 50)) == 0):
            sys.stdout.write("^")
            sys.stdout.flush()

def avg(agents):   
    sum = 0
    total_size = 0
    for a in agents:
        agents_on_island = a._AggregateAgent__agents.values()
        total_size += len(agents_on_island)
        for agent in agents_on_island:
            sum += agent.get_fitness()
    return sum * 1. / total_size