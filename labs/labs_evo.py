# coding=utf-8
import logging
import os
import datetime

from pyage.core import address
from pyage.core.agent.agent import generate_agents, Agent
from pyage.core.locator import RowLocator
from pyage.core.migration import  NoMigration
from pyage.core.statistics import SimpleStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.solutions.evolution.crossover import  AverageFloatCrossover
from pyage.solutions.evolution.evaluation import  FloatRastriginEvaluation
from pyage.solutions.evolution.initializer import  FloatInitializer
from pyage.solutions.evolution.mutation import  UniformFloatMutation
from pyage.solutions.evolution.selection import TournamentSelection

from labs.evaluation import LabsEvaluation
from labs.mutation import UniformLabsMutation
from labs.initializer import LabsInitializer
from labs.crossover import LabsSinglePointCrossover
from labs.gnuplot import LabsEvoStatistics


logger = logging.getLogger(__name__)

#Wydaje sie ze powinno byc 1 dla klasycznego ewolucyjnego
agents_count = int(os.environ['AGENTS'])
logger.debug("EVO, %s agents", agents_count)

agents = generate_agents("agent", agents_count, Agent)

step_limit = int(os.environ['STEP_LIMIT'])
stop_condition = lambda: StepLimitStopCondition(step_limit)


dims = int(os.environ['GENOTYPE_LENGTH'])  # Length of genotype 
size = int(os.environ['POPULATION_SIZE']) 
mutation_probability = float(os.environ['MUTATION_PROBABILITY']) 
tournament_size = int(os.environ['TOURNAMENT_SIZE'])
tournament_count = int(os.environ['TOURNAMENT_COUNT'])
# operators = lambda: [FloatRastriginEvaluation(), TournamentSelection(size=125, tournament_size=125),
#                     AverageFloatCrossover(size=size), UniformFloatMutation(probability=0.1, radius=1)]

# 
operators = lambda: [LabsEvaluation(), 
	TournamentSelection(size=tournament_count, tournament_size=tournament_size),
	LabsSinglePointCrossover(size = size),
	UniformLabsMutation(mutation_probability)]
#initializer = lambda: FloatInitializer(500, size, -10, 10)

initializer = lambda: LabsInitializer(dims = dims, size = size)

address_provider = address.SequenceAddressProvider

migration = NoMigration
# Locator do sprawdzenia
locator = RowLocator

stats = lambda: LabsEvoStatistics(
	step_limit=step_limit,
	header='LABS, evolutionary algorithm, genotype length %d' % dims,
	output_file_name='fitness_%s' % str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")))