# coding=utf-8
import logging
import os
import datetime

from pyage.core import address
from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.locator import RowLocator
from pyage.core.migration import ParentMigration
from pyage.core.stop_condition import StepLimitStopCondition
#from pyage.solutions.evolution.crossover import SinglePointCrossover
from pyage.solutions.evolution.evaluation import FloatRastriginEvaluation
from pyage.solutions.evolution.initializer import float_emas_initializer
from pyage.solutions.evolution.mutation import UniformFloatMutation

from labs.crossover import NoOpCrossover
from labs.crossover import NoOpRandomCrossover
from labs.crossover import LabsSinglePointCrossover
from labs.genotype import LabsGenotype
from labs.mutation import UniformLabsMutationWithStrength
from labs.mutation import MultiUniformLabsMutationWithStrength
from labs.mutation import RandomLabsMutationWithStrength
from labs.mutation import MemeticRandomLabsMutationWithStrength
from labs.initializer import labs_emas_initializer
from labs.evaluation import LabsEvaluation
from labs.gnuplot import LabsStatistics
from labs.emas import EmasService


logger = logging.getLogger(__name__)

island_count = int(os.environ['ISLANDS'])
logger.debug("EMAS, %s islands", island_count)
agents = unnamed_agents(island_count, AggregateAgent)

step_limit = int(os.environ['STEP_LIMIT'])
stop_condition = lambda: StepLimitStopCondition(step_limit)

#aggregated_agents = lambda: float_emas_initializer(40, energy=100, size=50, lowerbound=-10, upperbound=10)

dims = int(os.environ['GENOTYPE_LENGTH'])  # Length of genotype
energy = int(os.environ['STARTING_ENERGY'])  # Starting energy of agents
size = int(os.environ['AGENTS_PER_ISLAND'])  # Number of agents per island
aggregated_agents = lambda: labs_emas_initializer(dims, energy, size)

emas = EmasService

minimal_energy = lambda: 0
reproduction_minimum = lambda: 90
migration_minimum = lambda: 120
newborn_energy = lambda: 100
transferred_energy = lambda: 40
min_same_fitness_transferred_energy = lambda: 20
max_same_fitness_transferred_energy = lambda: 60

#evaluation = FloatRastriginEvaluation
evaluation = LabsEvaluation
crossover = LabsSinglePointCrossover
#mutation = lambda: UniformFloatMutation(probability=1, radius=1)

bad_mutation_probability = float(os.environ['BAD_MUTATION_PROBABILITY'])  # Length of genotype
min_strength = int(os.environ['MIN_MUTATION_STR'])  # Starting energy of agents
max_strength = int(os.environ['MAX_MUTATION_STR'])  # Number of agents per island
#sprawdzic dlaczego tu jest lambda
mutation = lambda: MemeticRandomLabsMutationWithStrength(\
	bad_mutation_probability = bad_mutation_probability, min_strength = min_strength, max_strength = max_strength, tree_width = 3, tree_height = 3) 
# mutation = lambda: MultiUniformLabsMutationWithStrength(\
	# bad_mutation_probability = bad_mutation_probability, min_strength = min_strength, max_strength = max_strength, multi = 2) 

address_provider = address.SequenceAddressProvider

migration = ParentMigration
locator = RowLocator

stats = lambda: LabsStatistics(
	step_limit=step_limit,
	header='LABS, EMAS with COMMA, genotype length %d' % dims,
	output_file_name='fitness_%s' % str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")))
