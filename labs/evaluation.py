from math import cos, pi, sin, sqrt
from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype

# Wylicza fitness genotypu za pomoca wskaznika zwanego merit factor
class LabsEvaluation(Operator):
    def __init__(self):
        super(LabsEvaluation, self).__init__()

    def process(self, population):
        for genotype in population:
            genotype.fitness = self.merit_factor(genotype.genes)
            
    # f(S) = L^2 / (2 * E(S))
    # E(S) = sum[k = 1 to L-k]((C[k](S))^2)
    # C(S) = sum[i = 1 to k](s[i]s[i+k])
    def merit_factor(self, genes): 
        E = 0
        L = len(genes)
        for k in xrange(1, L):
            Ck = 0
            for i in xrange(L - k - 1):
                Ck += genes[i] * genes[i + k]
            E += (Ck)**2
        #return -E
        return  L**2 / (2. * E)

