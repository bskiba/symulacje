from random import uniform
from random import randint
from pyage.core.emas import EmasAgent
from pyage.core.operator import Operator
from pyage.solutions.evolution.genotype import PointGenotype, FloatGenotype
from labs.genotype import LabsGenotype

# Tworzy genotyp - losowa stworzona liste jedynek i minus jedynek
class LabsInitializer(Operator):
    def __init__(self, dims=3, size=100):
        super(LabsInitializer, self).__init__(LabsGenotype)
        self.size = size
        self.dims = dims

    def process(self, population):
        for i in xrange(self.size):
            population.append(LabsGenotype([self.__randomize() for _ in xrange(self.dims)]))

    def __randomize(self):
        # randint randomly returns 0 or 1
        # we return -1 or 1
        if randint(0,1) == 0:
            return -1
        else: 
            return 1

def labs_emas_initializer(dims=2, energy=10, size=100):
    agents = {}
    population = []
    init = LabsInitializer(dims, size)
    init.process(population)
    for genotype in population:
        agent = EmasAgent(genotype, energy)
        agents[agent.get_address()] = agent
    return agents
